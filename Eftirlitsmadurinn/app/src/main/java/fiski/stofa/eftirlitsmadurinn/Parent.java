package fiski.stofa.eftirlitsmadurinn;

import java.util.ArrayList;
 
public class Parent
{
	private int id;
    private String londunarstadur;
    private String dagsetning;
    private String skip;
    private String vidtakandi;
     
    // ArrayList to store child objects
    private ArrayList<Child> children;
     
    
    public int getId()
    {
        return id;
    }
     
    public void setId(int id)
    {
        this.id = id;
    }
    
    public String getLondunarstadur()
    {
        return londunarstadur;
    }
     
    public void setLondunarstadur(String name)
    {
        this.londunarstadur = name;
    }
    
    public String getDagsetning()
    {
        return dagsetning.substring(11,16)+" "+dagsetning.substring(0,10);
    }
     
    public void setDagsetning(String text1)
    {
        this.dagsetning = text1;
    }
     
    public String getSkip()
    {
        return skip;
    }
     
    public void setSkip(String text2)
    {
        this.skip = text2;
    }
    
    public String getVidtakandi()
    {
        return vidtakandi;
    }
     
    public void setVidtakandi(String text2)
    {
        this.vidtakandi = text2;
    }
     
     
    // ArrayList to store child objects
    public ArrayList<Child> getChildren()
    {
        return children;
    }
     
    public void setChildren(ArrayList<Child> children)
    {
        this.children = children;
    }
}