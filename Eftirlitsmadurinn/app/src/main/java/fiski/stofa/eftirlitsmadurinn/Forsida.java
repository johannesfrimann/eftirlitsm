package fiski.stofa.eftirlitsmadurinn;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Forsida extends Activity {

    EditText userName, password;
    Gagnagrunnur grunnur = new Gagnagrunnur(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        boolean loggedIn = grunnur.areTablesEmpty();
        if (loggedIn)
        {
            Intent nextScreen = new Intent(getApplicationContext(), Flutningshluti.class);
            startActivity(nextScreen);
        }
        else
        {
		    setContentView(R.layout.login);
        }
	}
	
	public void Flutningshluti(View view)
	{
        userName = (EditText)findViewById(R.id.loginmail);
        password   = (EditText)findViewById(R.id.loginPassword);
        String user = userName.getText().toString();
        String pass = password.getText().toString();
        if (user == "" || user.isEmpty())
        {
            userName.setError("Nauðsynlegt er að slá inn notandanafn");
        }
        else if (pass == "" || pass.isEmpty())
        {
            password.setError("Nauðsynlegt er að sla inn notandanafn");
        }
        else
        {
            Gagnagrunnur grunnur = new Gagnagrunnur(this);
            grunnur.addUser(user, pass);
            Intent nextScreen = new Intent(getApplicationContext(), Flutningshluti.class);
            startActivity(nextScreen);
        }
	}

}
