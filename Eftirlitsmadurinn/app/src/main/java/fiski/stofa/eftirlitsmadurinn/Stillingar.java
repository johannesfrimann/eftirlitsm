package fiski.stofa.eftirlitsmadurinn;

import android.os.Bundle;
import android.preference.PreferenceActivity;
 
public class Stillingar extends PreferenceActivity {
 
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.stillingar);
 
    }
}
