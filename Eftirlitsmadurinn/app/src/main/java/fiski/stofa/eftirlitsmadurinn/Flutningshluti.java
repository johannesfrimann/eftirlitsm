package fiski.stofa.eftirlitsmadurinn;
 
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
 
public class Flutningshluti extends Activity {
 
  private Spinner vidtakandiFra, vidtakandiTil,londunarhofnFra, londunarhofnTil;
  private String vidtakandiFraValue, vidtakandiTilValue, londunarhofnFraValue, londunarhofnTilValue;
    RadioButton kennitalaRadio, postnumerRadio;
    EditText vidtakandiKennitala;
 
  @Override
  public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.flutningshluti);

	 }

    public void radioButtonsClicked(View view) {
        vidtakandiKennitala = (EditText)findViewById(R.id.kennitala);
        vidtakandiFra = (Spinner)findViewById(R.id.spinner3);
        vidtakandiTil = (Spinner)findViewById(R.id.spinner4);
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButton:
                if (checked)
                    vidtakandiKennitala.setEnabled(false);
                    vidtakandiFra.setEnabled(true);
                    vidtakandiTil.setEnabled(true);
                    break;
            case R.id.radioButton2:
                if (checked)
                    vidtakandiFra.setEnabled(false);
                    vidtakandiTil.setEnabled(false);
                    vidtakandiKennitala.setEnabled(true);
                    break;
        }
    }
  
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.forsida, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.action_settings:
                    Intent nextScreen2 = new Intent(getApplicationContext(), Stillingar.class);
                    startActivity(nextScreen2);
	        	return true;
            case R.id.action_logout:
                Gagnagrunnur grunnur = new Gagnagrunnur(this);
                grunnur.deleteUser();
                Intent logInScreen = new Intent(getApplicationContext(), Forsida.class);
                startActivity(logInScreen);
                return true;
	        default:
	            return false;
	    }
	}

    @Override
    public void onBackPressed() {
    }

  public void GetList(View view)
  {
      londunarhofnFra = (Spinner)findViewById(R.id.spinner1);
      londunarhofnTil = (Spinner)findViewById(R.id.spinner2);
      vidtakandiFra = (Spinner)findViewById(R.id.spinner3);
      vidtakandiTil = (Spinner)findViewById(R.id.spinner4);
      londunarhofnFraValue = getResources().getStringArray(R.array.leitarmoguleikar_values)[londunarhofnFra.getSelectedItemPosition()];
      londunarhofnTilValue = getResources().getStringArray(R.array.leitarmoguleikar_values)[londunarhofnTil.getSelectedItemPosition()];
      vidtakandiFraValue = getResources().getStringArray(R.array.leitarmoguleikar_values)[vidtakandiFra.getSelectedItemPosition()];
      vidtakandiTilValue = getResources().getStringArray(R.array.leitarmoguleikar_values)[vidtakandiTil.getSelectedItemPosition()];
	  Intent nextScreen = new Intent(getApplicationContext(), Flutningsgogn.class);
      nextScreen.putExtra("londunarhofnFra", londunarhofnFraValue);
      nextScreen.putExtra("londunarhofnTil", londunarhofnTilValue);
      nextScreen.putExtra("vidtakandiFra", vidtakandiFraValue);
      nextScreen.putExtra("vidtakandiTil", vidtakandiTilValue);
      startActivity(nextScreen);
  }
}
