package fiski.stofa.eftirlitsmadurinn;

public class Child
{
    private int id;
    private String veidarfaeri;
    private String fisktegund;
    private String umbudir;
    private String fjoldi;
    private String magn;
    private String stada;
    private int vigtarnotaNr;
    private String vidtakandiNafn;
     
    public int getId()
    {
        return id;
    }
     
    public void setId(int name)
    {
        this.id = name;
    }
    
    public String getVeidarfaeri()
    {
        return veidarfaeri;
    }
     
    public void setVeidarfaeri(String name)
    {
        this.veidarfaeri = name;
    }
    
    public String getFisktegund()
    {
        return fisktegund;
    }
     
    public void setFisktegund(String name)
    {
        this.fisktegund = name;
    }
    
    public String getUmbudir()
    {
        return umbudir;
    }
     
    public void setUmbudir(String name)
    {
        this.umbudir = name;
    }
    
    public String getFjoldi()
    {
        return fjoldi;
    }
     
    public void setFjoldi(String name)
    {
        this.fjoldi = name;
    }
    
    public String getMagn()
    {
        return magn;
    }
     
    public void setMagn(String name)
    {
        this.magn = name;
    }
    
    public String getStada()
    {
        return stada;
    }
     
    public void setStada(String name)
    {
        this.stada = name;
    }
    
    public int getVigtarnotaNr()
    {
        return vigtarnotaNr;
    }
     
    public void setVigtarnotaNr(int name)
    {
        this.vigtarnotaNr = name;
    }

    public String getVidtakandiNafn() { return vidtakandiNafn; }

    public void setVidtakandiNafn(String name)
    {
        this.vidtakandiNafn = name;
    }
    
    
     
}