package fiski.stofa.eftirlitsmadurinn;

import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Gagnagrunnur extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "UserInfo111";

    public Gagnagrunnur(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String userTableQuery = "CREATE TABLE " + "User " + "("
                + "id" + " INTEGER PRIMARY KEY,"
                + "userName" + " TEXT,"
                + "password" + " TEXT"
                + ")";
        db.execSQL(userTableQuery);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * Storing workout details in right table
     * */
    public void addUser(String userName, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("userName", userName);
        values.put("password", password);
        // Inserting Row
        db.insert("User", null, values);
        db.close(); // Closing database connection
    }

    /**
     * Storing workout details in right table
     * */
    public void deleteUser() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("User", null, null);
        db.close(); // Closing database connection
    }

    public boolean areTablesEmpty()
    {
        String selectQuery = "SELECT * FROM User";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        int rowCount = cursor.getCount();
        db.close();
        cursor.close();
        return rowCount>0;
    }

    public String getUserName()
    {
        String selectQuery = "SELECT * FROM User";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        cursor.moveToFirst();
        String UserName = cursor.getString(cursor.getColumnIndex("userName"));
        db.close();
        cursor.close();
        return UserName;
    }

    public String getPassword()
    {
        String selectQuery = "SELECT * FROM User";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        cursor.moveToFirst();
        String Password = cursor.getString(cursor.getColumnIndex("password"));
        db.close();
        cursor.close();
        return Password;
    }
}