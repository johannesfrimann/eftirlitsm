package fiski.stofa.eftirlitsmadurinn;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import java.lang.Runnable;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;

public class Flutningsgogn extends ExpandableListActivity
{
    private ArrayList<Parent> parents;
    JSONArray json;
    String[] upplysingar;
    String dagsetnTil, dagsetnFra;
    Gagnagrunnur grunnur = new Gagnagrunnur(this);
    int alertGluggi = 0;
    //Thread handler
    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new Runnable(){
        public void run() {
            if (json!= null)
            {
                results();
            }
            else if (alertGluggi == 1)
            {
                AlertDialog builder = new AlertDialog.Builder(Flutningsgogn.this)
                        .setTitle("Engin gögn...")
                        .setMessage("... uppfylltu leitarskilyrði þín. Prófaðu aftur með öðrum skilyrðum.")
                        .setPositiveButton("Til baka í leit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent prev = new Intent(getApplicationContext(), Flutningshluti.class);
                                startActivity(prev);
                            }
                        })
                        .show();
            }
            else if(alertGluggi == 2)
            {
                AlertDialog builder = new AlertDialog.Builder(Flutningsgogn.this)
                        .setTitle("Engin gögn...")
                        .setMessage("... uppfylltu leitarskilyrði þín. Prófaðu aftur með öðrum skilyrðum.")
                        .setPositiveButton("Til baka í leit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent prev = new Intent(getApplicationContext(), Flutningshluti.class);
                                startActivity(prev);
                            }
                        })
                        .show();
            }
            else if (alertGluggi ==3)
            {
                AlertDialog builder = new AlertDialog.Builder(Flutningsgogn.this)
                        .setTitle("Engin nettenging...")
                        .setMessage("... er í gangi eins og er. Kveiktu á nettengingunni og prófaðu svo aftur.")
                        .setPositiveButton("Reyna aftur", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                webCall();
                            }
                        })
                        .setNegativeButton("Til baka í leit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent prev = new Intent(getApplicationContext(), Flutningshluti.class);
                                startActivity(prev);
                            }
                        })
                        .show();
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Resources res = this.getResources();
        Drawable devider = res.getDrawable(R.drawable.linetrans);
        // Set ExpandableListView values
         
        getExpandableListView().setGroupIndicator(null);
        getExpandableListView().setDivider(devider);
        //getExpandableListView().setChildDivider(devider);
        getExpandableListView().setDividerHeight(1);
        registerForContextMenu(getExpandableListView());

        //Creating static data in arraylist
        Bundle extras = getIntent().getExtras();
        upplysingar = new String[4];
        if (extras != null)
        {
            upplysingar[0] = extras.getString("londunarhofnFra");
            upplysingar[1] = extras.getString("londunarhofnTil");
            upplysingar[2] = extras.getString("vidtakandiFra");
            upplysingar[3] = extras.getString("vidtakandiTil");
        }

        //saekja hversu marga daga aftur i timann a ad leita og stodu vigtarnotu
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        int dagar = prefs.getInt("fjSottraDaga", 2);
        //bua til dagsetningarformat
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dagsetnTil = dateFormat.format(Calendar.getInstance().getTime());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -dagar);
        dagsetnFra = dateFormat.format(cal.getTime());

        webCall();
    }


    protected void webCall()
    {
        Thread t = new Thread(){
            public void run(){


                String username = grunnur.getUserName();
                String password = grunnur.getPassword();
                String host = "eftirlittest.fiskistofa.is";
                String path = "http://"+host+"/landanir/leit/?vidtakandiKennitala=5307871769&londunarstadurPostnumerTil="+upplysingar[1]+"&londunarstadurPostnumerFra="+upplysingar[0]+"&dagsFra="+"2010-10-12"+"&dagsTil="+"2010-10-14";
                //Tetta er strengurinn eins og hann verdur i raunverulegum tjonustinnu(dagsetningarnar inni)
                //String path = "http://eftirlittest.fiskistofa.is/landanir/leit/?vidtakandiKennitala=5307871769&londunarstadurPostnumerTil="+upplysingar[1]+"&londunarstadurPostnumerFra="+upplysingar[0]+"&dagsFra="+dagsetnFra+"&dagsTil="+dagsetnTil;
                if (isNetworkAvailable())
                {
                try {

                    HttpClient client = new DefaultHttpClient();
                    AuthScope as;
                    as = new AuthScope(host, AuthScope.ANY_PORT);
                    UsernamePasswordCredentials upc = new UsernamePasswordCredentials(
                            username, password);
                    ((AbstractHttpClient) client).getCredentialsProvider()
                            .setCredentials(as, upc);
                    HttpContext localContext = new BasicHttpContext();
                    BasicScheme basicAuth = new BasicScheme();
                    localContext.setAttribute("preemptive-auth", basicAuth);
                    HttpHost targetHost = new HttpHost(host, AuthScope.ANY_PORT, "http");
                    HttpGet httpget = new HttpGet(path);
                    httpget.setHeader("Content-type", "application/json");
                    HttpResponse response = client.execute(targetHost,httpget,
                            localContext);
                    int status = response.getStatusLine().getStatusCode();
                    if (status == 400)
                    {
                        alertGluggi = 2;
                    }
                    else if (status == 404)
                    {
                        alertGluggi = 1;
                    }
                    HttpEntity entity = response.getEntity();

                    String content = EntityUtils.toString(entity,"UTF-8");
                    json = new JSONArray(content);
                }catch(Exception e)
                {
                    e.printStackTrace();
                    Log.d("Error: ", e.getMessage());
                }
                }
                else
                {
                    alertGluggi = 3;
                }
                mHandler.post(mUpdateResults);

            }
            };
            t.start();
    }

    private boolean isNetworkAvailable() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    /**
     * here should come your data service implementation
     * @return
     */
    private ArrayList<Parent> buildList()
    {
        // Creating ArrayList of type parent class to store parent class objects
        final ArrayList<Parent> list;
        list = new ArrayList<Parent>();
        try{

        for (int i =0 ;i <=json.length();i++)
        {

                JSONObject object = json.getJSONObject(i);

            //Create parent class object
            final Parent parent = new Parent();
             
              // Set values in parent class object
                      parent.setId(object.getInt("id"));
                      parent.setLondunarstadur(object.getString("londunarstadurHeiti"));
                      parent.setDagsetning(object.getString("londunHefst"));
                      parent.setVidtakandi(object.getString("londunarstadurPostnumer"));
                      parent.setSkip(object.getString("skipHeiti"));
                      parent.setChildren(new ArrayList<Child>());

                     JSONArray drilldown = object.getJSONArray("vigtarnotur");
                     for (int j = 0; j < drilldown.length(); j++)
                     {
                         JSONObject vigtarnota = drilldown.getJSONObject(j);
                      // Create Child class object 
                        final Child child = new Child();
                        child.setId(vigtarnota.getInt("id"));
                        child.setVeidarfaeri(vigtarnota.getString("veidarfaeriHeiti"));
                        child.setVidtakandiNafn(vigtarnota.getString("vidtakandiNafn"));
                        //Add Child class object to parent class object
                        parent.getChildren().add(child);
                     }
            //Adding Parent class object to ArrayList
            list.add(parent);
        }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Collections.reverse(list);
       return list;
    }

    public void results(){
        if (json != null)
        {
            ArrayList<Parent> data = buildList();
            loadHosts(data);
        }
    }

    private void loadHosts(final ArrayList<Parent> newParents)
    {
        if (newParents == null)
            return;

        parents = newParents;

        // Check for ExpandableListAdapter object
        if (this.getExpandableListAdapter() == null)
        {
            //Create ExpandableListAdapter Object
            final MyExpandableListAdapter mAdapter = new MyExpandableListAdapter();

            // Set Adapter to ExpandableList Adapter
            this.setListAdapter(mAdapter);
        }
        else
        {
            // Refresh ExpandableListView data
            ((MyExpandableListAdapter)getExpandableListAdapter()).notifyDataSetChanged();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent nextScreen2 = new Intent(getApplicationContext(), Stillingar.class);
                startActivity(nextScreen2);
                return true;
            case R.id.action_logout:
                Gagnagrunnur grunnur = new Gagnagrunnur(this);
                grunnur.deleteUser();
                Intent logInScreen = new Intent(getApplicationContext(), Forsida.class);
                startActivity(logInScreen);
                return true;
            default:
                return false;
        }
    }


    /**
     * A Custom adapter to create Parent view (Used grouprow.xml) and Child View((Used childrow.xml).
     */
    private class MyExpandableListAdapter extends BaseExpandableListAdapter
    {
         
 
        private LayoutInflater inflater;
 
        public MyExpandableListAdapter()
        {
            // Create Layout Inflater
            inflater = LayoutInflater.from(Flutningsgogn.this);
        }
     
         
        // This Function used to inflate parent rows view
         
        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, 
                View convertView, ViewGroup parentView)
        {
            final Parent parent = parents.get(groupPosition);
             
            // Inflate grouprow.xml file for parent rows
            convertView = inflater.inflate(R.layout.grouprow, parentView, false);
             
            // Get grouprow.xml file elements and set values
            ((TextView) convertView.findViewById(R.id.text1)).setText(parent.getDagsetning());
            ((TextView) convertView.findViewById(R.id.text)).setText(parent.getLondunarstadur());
            ((TextView) convertView.findViewById(R.id.text2)).setText(parent.getVidtakandi());
            ((TextView) convertView.findViewById(R.id.text3)).setText(parent.getSkip());

            return convertView;
        }
 
         
        // This Function used to inflate child rows view
        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, 
                View convertView, ViewGroup parentView)
        {
            final Parent parent = parents.get(groupPosition);
            final Child child = parent.getChildren().get(childPosition);
             
            // Inflate childrow.xml file for child rows
            convertView = inflater.inflate(R.layout.childrow, parentView, false);
             
            // Get childrow.xml file elements and set values
            //Settum bara inn nokkur prufugildi hér, má afkommenta að vild.
            //((TextView) convertView.findViewById(R.id.text)).setText(child.getFisktegund());
            //((TextView) convertView.findViewById(R.id.text1)).setText(child.getFjoldi());
            //((TextView) convertView.findViewById(R.id.text2)).setText(child.getMagn());
            //((TextView) convertView.findViewById(R.id.text3)).setText(child.getId());
            ((TextView) convertView.findViewById(R.id.text4)).setText(child.getVidtakandiNafn());
            //((TextView) convertView.findViewById(R.id.text5)).setText(child.getVigtarnotaNr());
            ((TextView) convertView.findViewById(R.id.text6)).setText(child.getVeidarfaeri());
             
            return convertView;
        }



        @Override
        public Object getChild(int groupPosition, int childPosition)
        {
            return parents.get(groupPosition).getChildren().get(childPosition);
        }
 
        //Call when child row clicked
        @Override
        
        public long getChildId(int groupPosition, int childPosition)
        {
            return childPosition;
        }
 
        @Override
        public int getChildrenCount(int groupPosition)
        {
            int size=0;
            if(parents.get(groupPosition).getChildren()!=null)
                size = parents.get(groupPosition).getChildren().size();
            return size;
        }
      
         
        @Override
        public Object getGroup(int groupPosition)
        {
            return parents.get(groupPosition);
        }
 
        @Override
        public int getGroupCount()
        {
            return parents.size();
        }
 
        //Call when parent row clicked
        @Override
        public long getGroupId(int groupPosition)
        {
            return groupPosition;
        }
 
        @Override
        public void notifyDataSetChanged()
        {
            // Refresh List rows
            super.notifyDataSetChanged();
        }
 
        @Override
        public boolean isEmpty()
        {
            return ((parents == null) || parents.isEmpty());
        }
 
        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition)
        {
            return true;
        }
 
        @Override
        public boolean hasStableIds()
        {
            return true;
        }
    }
}